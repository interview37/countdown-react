import { Dispatch, SetStateAction, useState, useRef, useEffect } from 'react';

const Countdown = () => {
  const [remainingTime, setRemainingTime]: [
    number,
    Dispatch<SetStateAction<number>>
  ] = useState<number>(0);
  const [updatedTime, setUpdatedTime]: [
    number,
    Dispatch<SetStateAction<number>>
  ] = useState<number>(0);
  const referenceTime = useRef<number>(0); // new Date().getTime()
  const intervalRef = useRef<number>(updatedTime);

  const decreaseTime = () =>
    setUpdatedTime((prevState) => {
      const difference = parseFloat(
        (Date.now() - referenceTime.current - 1000).toFixed(0)
      );
      if (prevState > 1) {
        return parseFloat(
          (
            (prevState * 1000 +
              referenceTime.current -
              Date.now() +
              difference) /
            1000
          ).toFixed(0)
        );
      } else {
        setRemainingTime(0);
        return 0;
      }
    });

  useEffect(() => {
    if (remainingTime > 0) {
      intervalRef.current = setInterval(
        decreaseTime,
        1000
      ) as unknown as number;
    }

    return () => clearInterval(intervalRef.current);
  }, [remainingTime]);

  return (
    <div className="App">
      <h1>{updatedTime}</h1>
      <button
        onClick={() => {
          referenceTime.current = Date.now();
          setRemainingTime(45);
          setUpdatedTime(45);
        }}
      >
        Set Start Value
      </button>
      <button
        onClick={() =>
          setUpdatedTime((prevState: any) => {
            if (prevState < 10) {
              return 0;
            } else {
              return prevState - 10;
            }
          })
        }
      >
        -10
      </button>
      <button
        onClick={() => setUpdatedTime((prevState: any) => prevState + 10)}
      >
        +10
      </button>
    </div>
  );
};

export default Countdown;
